using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Handles starting a game and mobile UI events
/// </summary>
public class MainMenuManager : NetworkBehaviour
{
    [SerializeField] private GameObject mainMenuButtons;
    [SerializeField] public UserUIVR userUIVR;

    private void Start()
    {
        CheckHasActiveUser();
    }

    [Command(requiresAuthority =false)]
    private void CheckHasActiveUser()
    {
        SetActivity(UserSystem.Instance.HasActiveUser);
    }

    [ClientRpc]
    private void SetActivity(bool hasActiveUser)
    {
        userUIVR.gameObject.SetActive(!hasActiveUser);
        mainMenuButtons.SetActive(hasActiveUser);
    }

    /// <summary>
    /// Loads a game experience with the given ID
    /// </summary>
    public void LoadMiniGame(int game)
    {
        switch (game)
        {
            case 0:
                if (isServer) CustomNetworkManager.singleton.ServerChangeScene("TangramSceneOnline");
                break;
            case 1:
                if (isServer) CustomNetworkManager.singleton.ServerChangeScene("ShootingGallerySceneOnline");
                break;
            case 2:
                if (isServer) CustomNetworkManager.singleton.ServerChangeScene("EndlessRunnerSceneOnline");
                break;
        }
    }

    [Command(requiresAuthority = false)]
    public void LogOutUserCommand()
    {
        LogOutUser();
    }

    /// <summary>
    /// Stops logging data. Logs out the user that was used for logging data.
    /// </summary>
    [ClientRpc]
    public void LogOutUser() 
    {
        mainMenuButtons.SetActive(false);
        UserSystem.Instance.HasActiveUser = false;
        userUIVR.gameObject.SetActive(true);

        if (!isServer) return;
        LoggerCommunicationProvider.Instance.StopLogging();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
