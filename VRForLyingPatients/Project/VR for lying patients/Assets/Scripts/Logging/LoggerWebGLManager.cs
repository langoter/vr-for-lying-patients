using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VrDashboardLogger.Editor.Classes;
using VrDashboardLogger.Editor;

public class LoggerWebGLManager : MonoBehaviour
{
    void Start()
    {
        var vrLogger = gameObject.AddComponent<VrLogger>();
        vrLogger.GetVrData(HandleVrData);
    }

    
    private void HandleVrData(VrData data)
    {
        Debug.Log(data);

        foreach (var record in data.records)
        {
            Debug.Log(record);                      

        }
    }


}


